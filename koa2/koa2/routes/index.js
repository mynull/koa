const router = require('koa-router')()
const log = require('../util/log.js')

router.get('/', async(ctx, next) => {
    await ctx.render('index', {
        title: 'Hello Koa 2!'
    });
})

router.get('/string', async(ctx, next) => {
    ctx.body = 'koa2 string'
})

router.get('/render', async(ctx, next) => {
    log('render', '12123')
    await ctx.render('index', { title: 'wanghao' })
})



module.exports = router