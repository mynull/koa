const router = require('koa-router')()
const { query } = require('../util/linkdb')
async function selectAllData() {
    let sql = 'SELECT name FROM ab'
    let data = await query(sql)
    return data
}
router.prefix('/users')
router.post('/post_test', async function(ctx, next) {


    let ctx_body = ctx.request.body
    console.log(ctx.method)
    ctx.body = {
        'name': await selectAllData(),
        'ctx_query': ctx.query,
        ctx_body

    }
})
router.get('/', async function(ctx, next) {
    console.log(ctx.method)
        //  从request找那个获取
    let request = ctx.request
    let req_query = request.query
    let req_querystring = request.querystring

    // 从上下文中直接获取
    let ctx_query = ctx.query
    let ctx_querystring = ctx.querystring
    console.log(ctx_query)
        // selectAllData 函数返回的还是promise
    ctx.body = {
        // 'name': await selectAllData(),
        // request,
        // req_query,
        // req_querystring,

        ctx_query,
        // ctx_querystring

    }
})

router.get('/bar', function(ctx, next) {
    ctx.body = 'this is a users/bar response'
})



module.exports = router