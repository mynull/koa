    const log4js = require('log4js');
    const Koa = require('koa')
    const app = new Koa()

    function log(f_name = 'index', f_log_msg = 2) {
        log4js.configure({
            appenders: [{
                type: 'console',
                category: "console"
            }, {
                type: "dateFile",
                filename: './logrecord/log',
                pattern: "_yyyyMMdd.log", //日期文件格式
                // absolute: false,
                alwaysIncludePattern: true,
                maxLogSize: 20480,
                backups: 3
                    // category: 'logInfo'	 //过滤功能
            }],
            replaceConsole: false, //替换console.log
            // levels: {
            //     logInfo: 'info',
            //     console: 'debug'
            // }
        });
        // app.use(log4js.connectLogger(log4js.getLogger('access'), { level: log4js.levels.INFO }));

        const logger = log4js.getLogger(f_name);
        logger.info(f_log_msg);
    }
    module.exports = log;

    // const logger = log4js.getLogger('logInfo');
    // const logger2 = log4js.getLogger('console');
    //
    //
    // logger2.debug('1');
    // logger.trace('2');
    // logger.debug('3');
    // logger.info('4');
    // logger.warn('5');
    // logger.error('6');
    // logger.fatal('7');
